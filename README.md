# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this? ###
This repository hosts the source code for the website www.joeriharleman.com. This website also has an API and a backend, which can be found at [joeriharleman-api](https://bitbucket.org/hb117/joeriharleman-api) and [joeriharleman-backend](https://bitbucket.org/hb117/joeriharleman-backend) respectively.
The client in this repository is an Angular2 application written in TypeScript.

That's all for now.